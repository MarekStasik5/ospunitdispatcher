package com.stach;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OsPunitDispatcherApplication {

	public static void main(String[] args) {
		SpringApplication.run(OsPunitDispatcherApplication.class, args);
	}
}

/*spring.h2.console.path=/h2
spring.datasource.url= jdbc:h2:mem:testdb
spring.datasource.username=sa
spring.datasource.password=
spring.datasource.driver-class-name=org.h2.Driver*/