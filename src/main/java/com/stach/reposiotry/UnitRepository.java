package com.stach.reposiotry;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stach.model.Unit;

@Repository
public interface UnitRepository extends JpaRepository<Unit, Long> {

	Optional<Unit> findByNameIgnoreCase(String replaceAll);

}
