package com.stach.reposiotry;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.stach.model.CallStatus;
import com.stach.model.Incident;

@Repository
public interface IncidentRepository extends JpaRepository<Incident, Long>{
		List<Incident> findAllByStatus(CallStatus status);
}
