package com.stach.model;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@Component
@SessionScope
public class IncidentUnit {

	private Incident incident;
	
	public Incident getIncident() {
		return incident;
	}
	
	public IncidentUnit() {
		clear();
	}
	
	public void clear() {
		incident=new Incident();
		incident.setStatus(CallStatus.TRIGGERED);
	}
			
	public void add(Unit unit) {
		incident.getUnits().add(unit);
	}	
	
}
