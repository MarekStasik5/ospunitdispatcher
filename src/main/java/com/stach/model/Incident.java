package com.stach.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Incident {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_incident")
	private Long id;
	private String localization;
	@Enumerated(EnumType.STRING)
	private CallStatus status;
	
	@ManyToMany
	@JoinTable(name="disposed_units",
	joinColumns=@JoinColumn(name="incident_id",referencedColumnName="id_incident"),
	inverseJoinColumns=@JoinColumn(name="unit_id",referencedColumnName="id_unit"))
	private Set<Unit>units=new HashSet<>();

	public Incident(String localization, CallStatus status, Set<Unit> units) {
		this.localization = localization;
		this.status = status;
		this.units = units;
	}
	
	public Incident() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLocalization() {
		return localization;
	}

	public void setLocalizationn(String localization) {
		this.localization = localization;
	}

	public CallStatus getStatus() {
		return status;
	}

	public void setStatus(CallStatus status) {
		this.status = status;
	}

	public Set<Unit> getUnits() {
		return units;
	}

	public void setUnits(Set<Unit> units) {
		this.units = units;
	}

	
}
