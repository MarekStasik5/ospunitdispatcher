package com.stach.model;

public enum CallStatus {
	TRIGGERED,
	IN_PROGRESS,
	COMPLETE;
	
	public static CallStatus nextStatus(CallStatus status) {
		if(status==TRIGGERED)
			return IN_PROGRESS;
		else if(status==IN_PROGRESS)
			return COMPLETE;
		else
			throw new IllegalArgumentException("No status for provided value");
	}
}
