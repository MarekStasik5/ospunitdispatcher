package com.stach.model;

import javax.persistence.*;

@Entity
public class Unit {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id_unit")
	private Long id;
	private String name;
	private String ability;
	private int waterTank;
	@Column(length=1024)
	private String equipment;
	private String imgPath;
	

	
	public Unit(String name, String ability, int waterTank, String equipment, String imgPath) {
		this.name = name;
		this.ability = ability;
		this.waterTank = waterTank;
		this.equipment = equipment;
		this.imgPath = imgPath;
	}

	public Unit() {}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAbility() {
		return ability;
	}

	public void setAbility(String ability) {
		this.ability = ability;
	}

	public int getWaterTank() {
		return waterTank;
	}

	public void setWaterTank(int waterTank) {
		this.waterTank = waterTank;
	}

	public String getEquipment() {
		return equipment;
	}

	public void setEquipment(String equipment) {
		this.equipment = equipment;
	}

	public String getImgPath() {
		return imgPath;
	}

	public void setImgPath(String imgPath) {
		this.imgPath = imgPath;
	}
	
	
	
	
}
