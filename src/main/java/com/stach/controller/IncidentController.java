package com.stach.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.stach.common.Message;
import com.stach.model.Incident;
import com.stach.model.IncidentUnit;
import com.stach.model.Unit;
import com.stach.reposiotry.IncidentRepository;
import com.stach.reposiotry.UnitRepository;

@Controller
public class IncidentController {

	private IncidentUnit  incidentUnit;
	private UnitRepository unitRepository;
	private IncidentRepository incidentRepository;
	
	@Autowired
	public IncidentController(IncidentUnit incidentUnit, UnitRepository unitRepository, IncidentRepository incidentRepository) {
		this.incidentUnit = incidentUnit;
		this.unitRepository = unitRepository;
		this.incidentRepository = incidentRepository;
	}
	
	@GetMapping("/dispatch/add")
	public String aggUnitToIncident(@RequestParam Long unitId, Model model) {
		Optional<Unit>unit=unitRepository.findById(unitId);
		unit.ifPresent(incidentUnit::add);
		if(unit.isPresent()) {
			model.addAttribute("message",new Message("Added","Added into incident: "+ unit.get().getName()));
		}else {
			model.addAttribute("message", new Message("Not added","Wrong unit id: "+ unitId));
		}
		return "message";
	}

	@GetMapping("/dispatch")
	public String getCurrentIncident(Model model) {
		model.addAttribute("incident",incidentUnit.getIncident());
		//model.addAttribute("sum",incidentUnit.getIncident().getUnits().stream());
		return "incident";
	}
	
	@PostMapping("/dispatch/realise")
	public String proceedIncident(@RequestParam String localization, Model model) {
		Incident incident=incidentUnit.getIncident();
		incident.setLocalizationn(localization);
		incidentRepository.save(incident);
		incidentUnit.clear();
		model.addAttribute("message", new Message("Thank you!","Units are calling"));
		return "message";
	}
	
	
	
}
