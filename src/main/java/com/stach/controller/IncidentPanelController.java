package com.stach.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.stach.model.CallStatus;
import com.stach.model.Incident;
import com.stach.reposiotry.IncidentRepository;

@Controller
public class IncidentPanelController {
	private IncidentRepository incidentRepository;

	public IncidentPanelController(IncidentRepository incidentRepository) {
		this.incidentRepository = incidentRepository;
	}
	
	@GetMapping("/panelIncidents")
	public String getIncident(@RequestParam(required=false)CallStatus status, Model model) {
		List<Incident>incidents;
		if(status==null)
			incidents=incidentRepository.findAll();
		else
			incidents=incidentRepository.findAllByStatus(status);
		model.addAttribute("incidents", incidents);
		return "panelIncidents";
	}
	
	private String singleOrderPanel(Incident incident, Model model) {
		model.addAttribute("incident",incident);
		return "detail/incident";
	}
	
	@GetMapping("/detail/incident/{id}")
	public String singleIncident(@PathVariable Long id, Model model) {
		Optional<Incident>incident=incidentRepository.findById(id);
		return incident.map(i->singleOrderPanel(i,model))
				.orElse("redirect:/");
	}

	@PostMapping("/detail/incident/{id}")
	public String changeIncidentStatus(@PathVariable Long id, Model model) {
		Optional<Incident>incident=incidentRepository.findById(id);
		incident.ifPresent(i->{
			i.setStatus(CallStatus.nextStatus(i.getStatus()));
			incidentRepository.save(i);
		});
		return incident.map(i->singleOrderPanel(i, model)).orElse("redirect/");
	}
	
}
