package com.stach.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.stach.model.Unit;
import com.stach.reposiotry.UnitRepository;

@Controller
public class UnitController {

	private UnitRepository unitRepository;

	@Autowired
	public UnitController(UnitRepository unitRepository) {
		this.unitRepository = unitRepository;
	}
	
	@GetMapping("/unit/{name}")
	public String getUnit(@PathVariable String name, Model model) {
		Optional<Unit>unit=unitRepository.findByNameIgnoreCase(name.replaceAll("-", " "));
		unit.ifPresent(it->model.addAttribute("unit",it));
		return unit.map(it->"unit").orElse("redirect:/");
	}
	
	
	
}
