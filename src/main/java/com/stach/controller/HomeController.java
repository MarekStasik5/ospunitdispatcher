package com.stach.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.stach.model.Unit;
import com.stach.reposiotry.UnitRepository;

@Controller
public class HomeController {
	private UnitRepository unitRepository;
	
	public HomeController(UnitRepository unitRepository) {
		this.unitRepository=unitRepository;
	}
	
	@GetMapping("/")
	public String home(Model model) {
		List<Unit>units=unitRepository.findAll();
		model.addAttribute("units",units);
		return "index";
	}
	
}
